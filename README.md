# FW-Http-Transport

## HTTP Transport Guzzle Adapter

```php
<?php
    require __DIR__.'/vendor/autoload.php';

    use FindWork\Transporter\Adapters\GuzzleAdapter;
    use GuzzleHttp\Client;

    $client = new GuzzleAdapter(new Client(['base_uri' => 'BASE URI']));

    $getMethod = $client->get();
?>
```